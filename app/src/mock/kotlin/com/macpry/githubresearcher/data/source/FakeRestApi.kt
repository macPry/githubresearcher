package com.macpry.githubresearcher.data.source

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.macpry.githubresearcher.data.model.api.repo.Repo
import com.macpry.githubresearcher.data.model.api.repo.RepoResponse
import com.macpry.githubresearcher.data.model.api.user.User
import com.macpry.githubresearcher.data.model.api.user.UserResponse
import com.macpry.githubresearcher.data.model.api.user_details.UserDetails
import com.macpry.githubresearcher.data.source.api.RestApi
import io.reactivex.Flowable
import retrofit2.Response

/* Copyright © Maciej Przybyl * All rights reserved. */

class FakeRestApi : RestApi {

    companion object {
        val repoList = mutableListOf<Repo>()
        val userList = mutableListOf<User>()
        val userDetails = UserDetails(name = "User Name", avatarUrl = "http://www.icare3d.org/images/AvatarTransp.png", followers = 22)

        init {
            (1..20).mapTo(repoList) { Repo("Repo $it", it.toLong()) }
            (1..20).mapTo(userList) { User("User $it", it.toLong()) }
        }
    }

    override fun repositories(query: String, sort: String, order: String): Flowable<Result<RepoResponse>> {
        val repoSearchResult = mutableListOf<Repo>()
        repoList.forEach { r ->
            if (r.name.contains(query)) {
                repoSearchResult.add(r)
            }
        }
        return Flowable.just(Result.response(Response.success(RepoResponse(repoSearchResult))))
    }

    override fun users(query: String, sort: String, order: String): Flowable<Result<UserResponse>> {
        val userSearchResult = mutableListOf<User>()
        userList.forEach { u ->
            if (u.name.contains(query)) {
                userSearchResult.add(u)
            }
        }
        return Flowable.just(Result.response(Response.success(UserResponse(userSearchResult))))
    }

    override fun userDetails(userId: Long): Flowable<Result<UserDetails>> =
            Flowable.just(Result.response(Response.success(userDetails)))
}