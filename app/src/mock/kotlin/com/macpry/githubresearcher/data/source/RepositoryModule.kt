package com.macpry.githubresearcher.data.source

import com.macpry.githubresearcher.data.source.api.RestApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/* Copyright © Maciej Przybyl * All rights reserved. */

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRestApi(): RestApi = FakeRestApi()
}