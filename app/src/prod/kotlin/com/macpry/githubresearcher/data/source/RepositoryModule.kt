package com.macpry.githubresearcher.data.source

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.macpry.githubresearcher.data.source.api.RestApi
import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import okhttp3.OkHttpClient

/* Copyright © Maciej Przybyl * All rights reserved. */

@Module
class RepositoryModule {

    companion object {
        val API_URL: HttpUrl = HttpUrl.parse("https://api.github.com/")
    }

    @Provides
    @Singleton
    fun provideApiUrl() = API_URL

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    @Singleton
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
            OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, apiUrl: HttpUrl): Retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(apiUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideRestApi(retrofit: Retrofit): RestApi = retrofit.create(RestApi::class.java)
}