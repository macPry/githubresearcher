package com.macpry.githubresearcher.common

/* Copyright © Maciej Przybyl * All rights reserved. */

interface BaseView<in T> {
    fun setPresenter(presenter: T)
}