@file:JvmName("Utils")

package com.macpry.githubresearcher.common

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView
import com.jakewharton.retrofit2.adapter.rxjava2.Result
import org.jetbrains.anko.getStackTraceString

/* Copyright © Maciej Przybyl * All rights reserved. */

fun AppCompatActivity.addFragmentToActivity(fragmentId: Int, fragment: Fragment) {
    checkNotNull(fragment)
    checkNotNull(supportFragmentManager)
    val fragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.add(fragmentId, fragment)
    fragmentTransaction.commit()
}

fun <T> Result<T>.isValid(): Boolean {
    if (!this.isError) {
        if (this.response().isSuccessful) {
            return true
        } else {
            error(this.response().errorBody().string())
        }
    } else {
        error(this.error().getStackTraceString())
    }
}

fun <T> RecyclerView.ViewHolder.onClick(list: List<T>, onItemClick: (T) -> Unit) {
    itemView.setOnClickListener {
        onItemClick.invoke(list[adapterPosition])
    }
}

fun TextView.textWatcher(searchTextWatcher: SearchTextWatcher.() -> Unit) {
    addTextChangedListener(SearchTextWatcher().apply(searchTextWatcher))
}

class SearchTextWatcher : TextWatcher {

    private var after: ((Editable?) -> Unit)? = null

    override fun afterTextChanged(s: Editable?) {
        after?.invoke(s)
    }

    fun afterTextChanged(listener: (Editable?) -> Unit) {
        after = listener
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }
}
