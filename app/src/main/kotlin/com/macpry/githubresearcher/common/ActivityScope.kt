package com.macpry.githubresearcher.common

import javax.inject.Scope

/* Copyright © Maciej Przybyl * All rights reserved. */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope