package com.macpry.githubresearcher.common

/* Copyright © Maciej Przybyl * All rights reserved. */

interface BasePresenter {
    fun subscribe()
    fun unsubscribe()
}