package com.macpry.githubresearcher

import android.app.Application
import com.macpry.githubresearcher.data.source.RepositoryModule

/* Copyright © Maciej Przybyl * All rights reserved. */

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this.applicationContext))
                .repositoryModule(RepositoryModule())
                .build()
    }
}