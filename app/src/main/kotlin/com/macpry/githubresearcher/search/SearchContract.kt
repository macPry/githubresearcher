package com.macpry.githubresearcher.search

import com.macpry.githubresearcher.common.BasePresenter
import com.macpry.githubresearcher.common.BaseView
import com.macpry.githubresearcher.data.model.GitResult

/* Copyright © Maciej Przybyl * All rights reserved. */

interface SearchContract {

    interface View : BaseView<Presenter> {
        fun showGitResult(gitResultList: MutableList<GitResult>)
        fun showError()
        fun showDetailsUI(userId: Long)
    }

    interface Presenter : BasePresenter {
        fun loadGitResult(query: String)
        fun openDetails(userId: Long)
    }
}