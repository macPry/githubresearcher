package com.macpry.githubresearcher.search

import com.macpry.githubresearcher.AppComponent
import com.macpry.githubresearcher.common.ActivityScope

import dagger.Component

/* Copyright © Maciej Przybyl * All rights reserved. */

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(SearchPresenterModule::class))
interface SearchPresenterComponent {
    fun inject(searchActivity: SearchActivity)
}