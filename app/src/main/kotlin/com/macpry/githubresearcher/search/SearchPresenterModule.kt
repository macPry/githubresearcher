package com.macpry.githubresearcher.search

import dagger.Module
import dagger.Provides

/* Copyright © Maciej Przybyl * All rights reserved. */

@Module
class SearchPresenterModule(private val searchView: SearchContract.View) {

    @Provides
    fun provideSearchView() = searchView
}