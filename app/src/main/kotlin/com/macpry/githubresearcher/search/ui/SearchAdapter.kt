package com.macpry.githubresearcher.search.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.common.onClick
import com.macpry.githubresearcher.data.model.GitResult
import com.macpry.githubresearcher.data.model.GitResultType
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find

/* Copyright © Maciej Przybyl * All rights reserved. */

class SearchAdapter(var gitResultList: MutableList<GitResult>, val onItemClick: (GitResult) -> Unit)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val repoType = GitResultType.REPO.ordinal
    val userType = GitResultType.USER.ordinal

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            repoType -> return RepoViewHolder(RepoUI().createView(AnkoContext.create(parent.context, parent)))
            userType -> {
                val userHolder = UserViewHolder(UserUI().createView(AnkoContext.create(parent.context, parent)))
                userHolder.onClick(gitResultList, onItemClick)
                return userHolder
            }
            else -> throw Exception("Invalid view holder type")
        }
    }

    override fun getItemViewType(position: Int): Int = gitResultList[position].type.ordinal

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val gitResponse = gitResultList[position]
        when (holder.itemViewType) {
            repoType -> {
                holder as RepoViewHolder
                holder.name.text = gitResponse.name
            }
            userType -> {
                holder as UserViewHolder
                holder.name.text = gitResponse.name
            }
        }
    }

    override fun getItemCount(): Int = gitResultList.size

    class RepoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.find(R.id.search_list_repo_name)
    }

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.find(R.id.search_list_user_name)
    }

    fun refresh(newGitResultList: MutableList<GitResult>) {
        gitResultList.clear()
        gitResultList.addAll(newGitResultList)
        notifyDataSetChanged()
    }
}