package com.macpry.githubresearcher.search

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.common.textWatcher
import com.macpry.githubresearcher.data.model.GitResult
import com.macpry.githubresearcher.details.DetailsActivity
import com.macpry.githubresearcher.search.ui.SearchAdapter
import com.macpry.githubresearcher.search.ui.SearchFragmentUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.intentFor
import kotlin.properties.Delegates

/* Copyright © Maciej Przybyl * All rights reserved. */

class SearchFragment : Fragment(), SearchContract.View {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private val searchAdapter = SearchAdapter(mutableListOf<GitResult>(),
            { gitResult -> searchPresenter.openDetails(gitResult.id) }
    )

    private var searchPresenter: SearchContract.Presenter by Delegates.notNull()

    override fun setPresenter(presenter: SearchContract.Presenter) {
        searchPresenter = checkNotNull(presenter)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            SearchFragmentUI(searchAdapter).createView(AnkoContext.create(context, this))

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val searchEditTest: EditText = find(R.id.search_edit_text)
        searchEditTest.textWatcher {
            afterTextChanged { text -> searchPresenter.loadGitResult(text.toString()) }
        }
    }

    override fun onResume() {
        super.onResume()
        searchPresenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        searchPresenter.unsubscribe()
    }

    override fun showGitResult(gitResultList: MutableList<GitResult>) {
        searchAdapter.refresh(gitResultList)
    }

    override fun showError() {
        Snackbar.make(view!!, getString(R.string.search_error_message), Snackbar.LENGTH_LONG).show()
    }

    override fun showDetailsUI(userId: Long) {
        startActivity(intentFor<DetailsActivity>().putExtra(DetailsActivity.EXTRA_USER_ID, userId))
    }
}