package com.macpry.githubresearcher.search

import com.macpry.githubresearcher.data.model.GitResult
import com.macpry.githubresearcher.data.source.Repository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/* Copyright © Maciej Przybyl * All rights reserved. */

class SearchPresenter @Inject constructor(
        private val repository: Repository, private val searchView: SearchContract.View)
    : SearchContract.Presenter {

    val compositeDisposable = CompositeDisposable()

    @Inject fun setupListeners() = searchView.setPresenter(this)

    override fun subscribe() {
        //Empty
    }

    override fun unsubscribe() {
        compositeDisposable.clear()
    }

    override fun loadGitResult(query: String) {
        compositeDisposable.clear()
        val disposable = repository.getGitResult(query)
                .subscribe(
                        {
                            processGitResult(it)
                        },
                        {
                            searchView.showError()
                        }
                )
        compositeDisposable.add(disposable)
    }

    private fun processGitResult(gitResult: MutableList<GitResult>) {
        searchView.showGitResult(gitResult)
    }

    override fun openDetails(userId: Long) {
        searchView.showDetailsUI(userId)
    }
}