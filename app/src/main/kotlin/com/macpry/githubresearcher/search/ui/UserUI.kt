package com.macpry.githubresearcher.search.ui

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.macpry.githubresearcher.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

/* Copyright © Maciej Przybyl * All rights reserved. */

class UserUI : AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {

        linearLayout {
            lparams {
                width = matchParent
                height = wrapContent
            }

            cardView {
                lparams {
                    width = matchParent
                    height = wrapContent
                }
                useCompatPadding = true

                linearLayout {
                    orientation = LinearLayout.VERTICAL

                    textView {
                        lparams {
                            margin = 5
                        }
                        textSize = 20F
                        id = R.id.search_list_user_name
                    }
                }
            }
        }
    }
}