package com.macpry.githubresearcher.search.ui

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.macpry.githubresearcher.R
import org.jetbrains.anko.*

/* Copyright © Maciej Przybyl * All rights reserved. */

class RepoUI : AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {

        linearLayout {
            lparams {
                width = matchParent
                height = wrapContent
            }
            orientation = LinearLayout.VERTICAL

            textView {
                lparams {
                    margin = 5
                }
                textSize = 20F
                id = R.id.search_list_repo_name
            }
        }
    }
}