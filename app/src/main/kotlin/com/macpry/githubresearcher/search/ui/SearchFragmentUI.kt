package com.macpry.githubresearcher.search.ui

import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.search.SearchFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

/* Copyright © Maciej Przybyl * All rights reserved. */

class SearchFragmentUI(val searchAdapter: SearchAdapter) : AnkoComponent<SearchFragment> {

    override fun createView(ui: AnkoContext<SearchFragment>): View = with(ui) {

        verticalLayout {
            lparams {
                width = matchParent
                height = matchParent
            }

            linearLayout {
                lparams {
                    width = matchParent
                    height = wrapContent
                }
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    gravity = Gravity.CENTER_VERTICAL
                    imageResource = android.R.drawable.ic_menu_search
                    padding = 5
                }

                editText {
                    lparams {
                        width = matchParent
                    }
                    id = R.id.search_edit_text
                }
            }

            recyclerView {
                lparams {
                    width = matchParent
                    height = matchParent
                }
                adapter = searchAdapter
                layoutManager = LinearLayoutManager(ctx)
            }
        }
    }
}