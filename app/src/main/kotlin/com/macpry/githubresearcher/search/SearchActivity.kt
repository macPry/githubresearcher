package com.macpry.githubresearcher.search

/* Copyright © Maciej Przybyl * All rights reserved. */

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.macpry.githubresearcher.App
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.common.addFragmentToActivity
import com.macpry.githubresearcher.search.ui.SearchActivityUI
import org.jetbrains.anko.setContentView
import javax.inject.Inject

class SearchActivity : AppCompatActivity() {

    val searchFragmentContainerId = R.id.search_container

    @Inject lateinit var searchPresenter: SearchPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        SearchActivityUI().setContentView(this)

        val searchFragment: SearchFragment
        if (supportFragmentManager.findFragmentById(searchFragmentContainerId) == null) {
            searchFragment = SearchFragment.newInstance()
            addFragmentToActivity(searchFragmentContainerId, searchFragment)
        } else {
            searchFragment = supportFragmentManager.findFragmentById(searchFragmentContainerId) as SearchFragment
        }

        DaggerSearchPresenterComponent.builder()
                .appComponent(App.appComponent)
                .searchPresenterModule(SearchPresenterModule(searchFragment))
                .build()
                .inject(this)
    }
}
