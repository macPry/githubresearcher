package com.macpry.githubresearcher.search.ui

import android.view.View
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.search.SearchActivity
import org.jetbrains.anko.*

/* Copyright © Maciej Przybyl * All rights reserved. */

class SearchActivityUI : AnkoComponent<SearchActivity> {

    override fun createView(ui: AnkoContext<SearchActivity>): View = with(ui) {

        frameLayout {
            lparams {
                width = matchParent
                height = matchParent
            }
            id = R.id.search_container
        }
    }
}