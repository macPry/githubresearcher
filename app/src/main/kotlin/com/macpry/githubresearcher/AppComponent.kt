package com.macpry.githubresearcher

import android.content.Context
import com.macpry.githubresearcher.data.source.Repository
import com.macpry.githubresearcher.data.source.RepositoryModule
import dagger.Component
import javax.inject.Singleton

/* Copyright © Maciej Przybyl * All rights reserved. */

@Singleton
@Component(modules = arrayOf(AppModule::class, RepositoryModule::class))
interface AppComponent {
    fun getContext(): Context
    fun getRepository(): Repository
}