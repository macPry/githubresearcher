package com.macpry.githubresearcher

import android.content.Context
import dagger.Module
import dagger.Provides

/* Copyright © Maciej Przybyl * All rights reserved. */

@Module
class AppModule(private val context: Context) {

    @Provides
    fun provideContest() = context
}