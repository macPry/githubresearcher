package com.macpry.githubresearcher.details

import com.macpry.githubresearcher.AppComponent
import com.macpry.githubresearcher.common.ActivityScope
import dagger.Component

/* Copyright © Maciej Przybyl * All rights reserved. */

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(DetailsPresenterModule::class))
interface DetailsPresenterComponent {
    fun inject(detailsActivity: DetailsActivity)
}