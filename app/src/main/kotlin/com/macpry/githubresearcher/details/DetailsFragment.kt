package com.macpry.githubresearcher.details

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.data.model.api.user_details.UserDetails
import com.macpry.githubresearcher.details.ui.DetailsFragmentUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.find
import kotlin.properties.Delegates

/* Copyright © Maciej Przybyl * All rights reserved. */

class DetailsFragment : Fragment(), DetailsContract.View {

    companion object {
        private val ARGUMENT_DETAILS_FRAGMENT_USER_ID = "USER_ID"

        fun newInstance(userId: Long): DetailsFragment {
            val arguments: Bundle = Bundle()
            arguments.putLong(DetailsFragment.ARGUMENT_DETAILS_FRAGMENT_USER_ID, userId)
            val detailsFragment: DetailsFragment = DetailsFragment()
            detailsFragment.arguments = arguments
            return detailsFragment
        }
    }

    private var detailsPresenter: DetailsContract.Presenter by Delegates.notNull()

    override fun setPresenter(presenter: DetailsContract.Presenter) {
        detailsPresenter = checkNotNull(presenter)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            DetailsFragmentUI().createView(AnkoContext.create(context, this))

    override fun onResume() {
        super.onResume()
        detailsPresenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        detailsPresenter.unsubscribe()
    }

    override fun showUserDetails(userDetails: UserDetails) {
        val name = find<TextView>(R.id.details_name)
        name.text = name.text.toString().plus(userDetails.name)

        val image = find<ImageView>(R.id.details_avatar)
        Glide.with(this).load(userDetails.avatarUrl).fitCenter().into(image)

        val followers = find<TextView>(R.id.details_followers)
        followers.text = followers.text.toString().plus(userDetails.followers.toString())
    }

    override fun showError() {
        Snackbar.make(view!!, getString(R.string.details_error_message), Snackbar.LENGTH_LONG).show()
    }
}