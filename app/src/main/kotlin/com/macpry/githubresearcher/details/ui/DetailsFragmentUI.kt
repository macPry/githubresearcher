package com.macpry.githubresearcher.details.ui

import android.view.View
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.details.DetailsFragment
import org.jetbrains.anko.*

/* Copyright © Maciej Przybyl * All rights reserved. */

class DetailsFragmentUI : AnkoComponent<DetailsFragment> {

    override fun createView(ui: AnkoContext<DetailsFragment>): View = with(ui) {

        scrollView {
            lparams {
                width = matchParent
                height = matchParent
            }

            verticalLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                }

                textView {
                    lparams {
                        margin = 10
                        padding = 10
                    }
                    id = R.id.details_name
                    text = context.getString(R.string.details_name_title)
                    textSize = 20F
                }

                imageView {
                    id = R.id.details_avatar
                }

                textView {
                    lparams {
                        margin = 10
                        padding = 10
                    }
                    id = R.id.details_followers
                    text = context.getString(R.string.details_followers_title)
                    textSize = 20F
                }
            }
        }
    }
}