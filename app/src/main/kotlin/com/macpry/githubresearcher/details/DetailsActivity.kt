package com.macpry.githubresearcher.details

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.macpry.githubresearcher.App
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.common.addFragmentToActivity
import com.macpry.githubresearcher.details.ui.DetailsActivityUI
import org.jetbrains.anko.setContentView
import javax.inject.Inject

/* Copyright © Maciej Przybyl * All rights reserved. */

class DetailsActivity : AppCompatActivity() {

    companion object {
        val EXTRA_USER_ID = "USER_ID"
    }

    val detailsFragmentContainerId = R.id.details_container

    @Inject lateinit var detailsPresenter: DetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DetailsActivityUI().setContentView(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val userId = intent.getLongExtra(EXTRA_USER_ID, 0)

        val detailsFragment: DetailsFragment
        if (supportFragmentManager.findFragmentById(detailsFragmentContainerId) == null) {
            detailsFragment = DetailsFragment.newInstance(userId)
            addFragmentToActivity(detailsFragmentContainerId, detailsFragment)
        } else {
            detailsFragment = supportFragmentManager.findFragmentById(detailsFragmentContainerId) as DetailsFragment
        }

        DaggerDetailsPresenterComponent.builder()
                .appComponent(App.appComponent)
                .detailsPresenterModule(DetailsPresenterModule(detailsFragment, userId))
                .build()
                .inject(this)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}