package com.macpry.githubresearcher.details

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.macpry.githubresearcher.common.isValid
import com.macpry.githubresearcher.data.model.api.user_details.UserDetails
import com.macpry.githubresearcher.data.source.Repository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/* Copyright © Maciej Przybyl * All rights reserved. */

class DetailsPresenter @Inject constructor(private val userId: Long,
                                           private val repository: Repository,
                                           private val detailsView: DetailsContract.View)
    : DetailsContract.Presenter {

    val compositeDisposable = CompositeDisposable()

    @Inject fun setupListeners() = detailsView.setPresenter(this)

    override fun subscribe() {
        loadUserDetails()
    }

    override fun unsubscribe() {
        compositeDisposable.clear()
    }

    override fun loadUserDetails() {
        compositeDisposable.add(repository.getUserDetails(userId)
                .subscribe(
                        {
                            result ->
                            if (result.isValid()) {
                                processUserDetails(result)
                            }
                        },
                        {
                            detailsView.showError()
                        }
                )
        )
    }

    private fun processUserDetails(userResult: Result<UserDetails>) {
        detailsView.showUserDetails(userResult.response().body())
    }
}