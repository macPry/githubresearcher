package com.macpry.githubresearcher.details.ui

import android.view.View
import com.macpry.githubresearcher.R
import com.macpry.githubresearcher.details.DetailsActivity
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent

/* Copyright © Maciej Przybyl * All rights reserved. */

class DetailsActivityUI : AnkoComponent<DetailsActivity> {

    override fun createView(ui: AnkoContext<DetailsActivity>): View = with(ui) {

        frameLayout {
            lparams {
                width = matchParent
                height = matchParent
            }
            id = R.id.details_container
        }
    }
}