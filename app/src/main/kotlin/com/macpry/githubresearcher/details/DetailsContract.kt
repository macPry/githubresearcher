package com.macpry.githubresearcher.details

import com.macpry.githubresearcher.common.BasePresenter
import com.macpry.githubresearcher.common.BaseView
import com.macpry.githubresearcher.data.model.api.user_details.UserDetails

/* Copyright © Maciej Przybyl * All rights reserved. */

interface DetailsContract {

    interface View : BaseView<Presenter> {
        fun showUserDetails(userDetails: UserDetails)
        fun showError()
    }

    interface Presenter : BasePresenter {
        fun loadUserDetails()
    }
}