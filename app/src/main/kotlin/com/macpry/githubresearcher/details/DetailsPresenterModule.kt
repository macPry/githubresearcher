package com.macpry.githubresearcher.details

import dagger.Module
import dagger.Provides

/* Copyright © Maciej Przybyl * All rights reserved. */

@Module
class DetailsPresenterModule(
        private val detailsView: DetailsContract.View, private val userId: Long) {

    @Provides
    fun provideDetailsView() = detailsView

    @Provides
    fun provideUserId() = userId
}