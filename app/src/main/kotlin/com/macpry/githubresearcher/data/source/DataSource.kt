package com.macpry.githubresearcher.data.source

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.macpry.githubresearcher.data.model.GitResult
import com.macpry.githubresearcher.data.model.api.user_details.UserDetails
import io.reactivex.Flowable

/* Copyright © Maciej Przybyl * All rights reserved. */

interface DataSource {

    fun getGitResult(query: String): Flowable<MutableList<GitResult>>
    fun getUserDetails(userId: Long): Flowable<Result<UserDetails>>
}