package com.macpry.githubresearcher.data.model.api.user

import com.google.gson.annotations.SerializedName

/* Copyright © Maciej Przybyl * All rights reserved. */

data class UserResponse(

        @SerializedName("items")
        var userList: List<User>
)