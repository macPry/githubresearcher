package com.macpry.githubresearcher.data.source.api

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.macpry.githubresearcher.data.model.api.repo.RepoResponse
import com.macpry.githubresearcher.data.model.api.user.UserResponse
import com.macpry.githubresearcher.data.model.api.user_details.UserDetails
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/* Copyright © Maciej Przybyl * All rights reserved. */

interface RestApi {

    @GET("search/repositories")
    fun repositories(
            @Query("q") query: String,
            @Query("sort") sort: String = "id",
            @Query("order") order: String = "asc")
            : Flowable<Result<RepoResponse>>

    @GET("search/users")
    fun users(
            @Query("q") query: String,
            @Query("sort") sort: String = "id",
            @Query("order") order: String = "asc")
            : Flowable<Result<UserResponse>>

    @GET("user/{id}")
    fun userDetails(@Path("id") userId: Long): Flowable<Result<UserDetails>>
}