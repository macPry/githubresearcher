package com.macpry.githubresearcher.data.model

/* Copyright © Maciej Przybyl * All rights reserved. */

data class GitResult(

        val type: GitResultType,
        val name: String,
        val id: Long
)