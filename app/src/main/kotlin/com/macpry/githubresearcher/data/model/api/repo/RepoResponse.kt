package com.macpry.githubresearcher.data.model.api.repo

import com.google.gson.annotations.SerializedName

/* Copyright © Maciej Przybyl * All rights reserved. */

data class RepoResponse(

        @SerializedName("items")
        var repoList: List<Repo>
)