package com.macpry.githubresearcher.data.model

/* Copyright © Maciej Przybyl * All rights reserved. */

enum class GitResultType {
    REPO, USER
}