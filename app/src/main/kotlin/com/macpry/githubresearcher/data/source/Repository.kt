package com.macpry.githubresearcher.data.source

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.macpry.githubresearcher.common.isValid
import com.macpry.githubresearcher.data.model.GitResult
import com.macpry.githubresearcher.data.model.GitResultType
import com.macpry.githubresearcher.data.model.api.repo.RepoResponse
import com.macpry.githubresearcher.data.model.api.user.UserResponse
import com.macpry.githubresearcher.data.model.api.user_details.UserDetails
import com.macpry.githubresearcher.data.source.api.RestApi
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/* Copyright © Maciej Przybyl * All rights reserved. */

@Singleton
class Repository @Inject constructor(private val restApi: RestApi) : DataSource {

    override fun getGitResult(query: String): Flowable<MutableList<GitResult>> {
        if (query == "") {
            return Flowable.just(mutableListOf<GitResult>())
                    .observeOn(AndroidSchedulers.mainThread())
        }

        val reposFlowable = restApi.repositories(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        val usersFlowable = restApi.users(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        return Flowable.zip(reposFlowable, usersFlowable,
                BiFunction<Result<RepoResponse>, Result<UserResponse>, MutableList<GitResult>> {
                    repoResponseResult, userResponseResult ->
                    createGitResultList(repoResponseResult, userResponseResult)
                })
                .subscribeOn(Schedulers.io())
                .delay(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun createGitResultList(
            repoResult: Result<RepoResponse>, userResult: Result<UserResponse>): MutableList<GitResult> {
        val gitResultList = mutableListOf<GitResult>()
        if (repoResult.isValid()) {
            repoResult.response().body().repoList.forEach { repo ->
                gitResultList.add(GitResult(type = GitResultType.REPO, name = repo.name, id = repo.id))
            }
        }
        if (userResult.isValid()) {
            userResult.response().body().userList.forEach { user ->
                gitResultList.add(GitResult(type = GitResultType.USER, name = user.name, id = user.id))
            }
        }
        gitResultList.sortBy { it.id }
        return gitResultList
    }

    override fun getUserDetails(userId: Long): Flowable<Result<UserDetails>> {
        checkNotNull(userId)
        return restApi.userDetails(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
