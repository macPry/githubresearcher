package com.macpry.githubresearcher.data.model.api.user_details

import com.google.gson.annotations.SerializedName

/* Copyright © Maciej Przybyl * All rights reserved. */

data class UserDetails(

        @SerializedName("login")
        val name: String,

        @SerializedName("avatar_url")
        val avatarUrl: String,

        @SerializedName("followers")
        val followers: Long
)