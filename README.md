# **GitHubResearcher** #
Android app (written in Kotlin) allows to search for GitHub userList and repositories.
## Installation ##
Clone the project from repository. Use your Git client and paste following command:
```
$ git clone https://macPry@bitbucket.org/macPry/githubresearcher.git
```
Then open the project with your Android IDE and run it.
## Usage ##

## Known bugs ##

## Contributing ##
1. Open an issue first to discuss potential changes/additions.
2. Create a topic branch from develop.
3. Make some commits to improve the project.
4. Push this branch to Bitbucket project.
5. Open a Pull Request on Bitbucket.
6. Discuss, and optionally continue committing.
7. The project owner merges or closes the Pull Request.
## License ##
Copyright 2016 Maciej Przybyl

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.